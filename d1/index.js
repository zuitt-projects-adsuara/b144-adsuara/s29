const express = require("express");

//Create an application using express 
//This creates an express application and stores this in a constant called app
const app = express();
const port = 4000 

//Middleware => it is a software that provides common services and capabilities to applications outside of what's offered by the operating system 
//API management is one of the common application middleware
app.use(express.json());//Allows your app to read json data
app.use(express.urlencoded({extended:true}))//Allows your app to received any other data type such as object, boolean etc. which we will use throughout our application 


//Routes
//This route expects to receive a GET request at the base URI "/"
//https://localhost:4000/

app.get("/", (req, res) => {
res.send("Hello World")

})


//this route expects to receive a GET request at the URI "/hello"
app.get("/hello/kitty", (req, res) => {
res.send("Hello from the /hello endpoint")

})

//This route expects to receive a POST request at the URI "/hello" which receives data of firstName and lastName 
app.post("/hello", (req, res) => {
     //req.body contains the contents/data of the request body
     // all the properties defined in our Postman request will be accessible here as properties with the same name
     res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`) 
})

// This route expect to receive a POST request at the URI "/signUp"

//this will create a user object int the "users" variable that mirrors a real world registration process

// mock database
let users = [];
app.post("/signUp",(req,res) => {
 
   //req.body contains the data from the postman client
   //If contents of the "req.body" with the property "username" and "password" is not empty
   if(req.body.username !=='' && req.body.password !==''){
      //This will store the user object sent via postman to the user array created above
      users.push(req.body);
      //Sends a response back to the postman
      res.send(`User ${req.body.username} successfully registered!`)
   }else{
      res.send("Please input Both username and password.")
   }
})

//This route to receive a PUT request at the URI "/change-password"
//This will update the password of a user that matches the information provided in the client
app.put("/change-password", (req,res) => {
	let message;
	//1. Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i<users.length; i++){
       //if the username provided in the client and the username of the object in the loop is the same 
       if(req.body.username == users[i].username){
       	//Change the password of the user found by the loop into the password provided in the client
       	users[i].password=req.body.password;
       	//Changes the message to be sent back by the response
       message = `User ${req.body.username}'s password has been updated`
      //Breaks out the loop once a user that matches the username provided in the client is found
      break;
       }else{
       	//if no user was found
       	message="User does not exist";
       }
	}
	//sends a response back to the client once the password has been updated or if user i not found
res.send(message)
})




app.listen(port, () => console.log(`Server running at ${port}`))

